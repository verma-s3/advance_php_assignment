<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Book Detial</title>
  <style type="text/css">
  	.media-content{
  		padding-left: 30px;
  	}
  	span{
  		font-size: 16px;
  		font-weight: bold;
  		padding-right: 25px;
  	}
  	.subtitle{
  		font-size: 24px;
  		font-weight: bold;
  	}
  </style>
</head>
<body>
	<h1 class="title">BookDetail</h1>
	<!-- displaying book data in table form -->
  <div class="box">
  	<article class="media">
	  	<div class="media-left">
		    <figure class="image is-128x128">
				<img src="images/covers/<?=htmlentities($book['image'])?>">
			</figure>
		  </div>  
	    <div class="media-content">
	      <h2 class="subtitle"><?=htmlentities($book['title'])?></h2>
	      <table>
	        <tr>
	          <td><span>Author: </span></td>
	          <td><?=htmlentities($book['author'])?></td>
	        </tr>
	        <tr>
	          <td><span>Publisher: </span></td>
	          <td><?=htmlentities($book['publisher'])?></td>
	        </tr>
	        <tr>
	          <td><span>Genre: </span></td>
	          <td><?=htmlentities($book['genre'])?></td>
	        </tr>
	        <tr>
	          <td><span>Price: </span></td>
	          <td>$<?=htmlentities($book['price'])?></td>
	        </tr>
	        <tr>
	          <td><span>Pages: </span></td>
	          <td><?=htmlentities($book['num_pages'])?></td>
	        </tr>
	        <tr>
	        	<td><span>Year: </span></td>
	          <td><?=htmlentities($book['year_published'])?></td>
	        </tr>
	        <tr>
	        	<td><span>In-Print: </span></td>
	          <td><?=htmlentities($book['in_print']==1?'Yes':'No')?></td>
	        </tr>
	        <tr>
	        	<td><span>Description: </span></td>
	          <td><?=strip_tags($book['description'])?></td>
	        </tr>
	      </table>
	    </div> 
    </article>        
  </div>
</body>
</html>