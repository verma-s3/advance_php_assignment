<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Booklist</title>
</head>
<body>
	<h1 class="title">Booklist</h1>
	<!-- displaying book data in table form -->
	<table class="table is-hoverable is-fullwidth is-striped">
	<!-- heading of book data -->
	<tr>	
		<th>Title</th>
		<th>Author</th>
		<th>Publisher</th>
		<th>Total Pages</th>
		<th>Price</th>
		<th>Genre</th>
		
	</tr>
	<!-- foreach starting for displaying book list and books -->
	<?php foreach ($result as $book): ?>
	<tr>
		<!-- using the anchor tag to displaying the detail page -->
		<td><a onclick="openDetail(this); return false;" data-book_id="<?=htmlspecialchars($book['book_id'], ENT_QUOTES)?>" href="#"><?=htmlspecialchars($book['title'])?></a></td>
		<td><?=htmlentities($book['author'])?></td>
		<td><?=htmlentities($book['publisher'])?></td>
		<td><?=htmlentities($book['num_pages'])?></td>
		<td><?=htmlentities($book['price'])?></td>
		<td><?=htmlentities($book['genre'])?></td>
	</tr>
	<?php endforeach; ?>
	<!-- end of for each loop -->
	</table>
</body>
</html>