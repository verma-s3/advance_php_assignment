<?php
ob_start();

session_start();

ini_set('display_errors',1);
ini_set('error_reporting',E_ALL);

// $dbh = new PDO('mysql:host=localhost;dbname=booksite', 'root', 'root');
$dbh = new PDO('sqlite:' . __DIR__ . '/../inc/booksite.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// check for search keyword from databse 
if(!empty($_GET['searching_data']) && !isset($_GET['search_result'])){
    $query = 'SELECT * from book where title LIKE :searching_data';
    $params = array(':searching_data'=> "%".$_GET["searching_data"]."%");
    $stmt = $dbh->prepare($query);
    $stmt->execute($params);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // check for the count in results
    if(count($results)){
        echo "<ul>";
        // foreach loop for displying book data
        foreach ($results as $book) {
            echo "<li data-book_id='{$book['book_id']}'><a onclick='openDetail(this); return false;' data-book_id='{$book['book_id']}'>{$book['title']}</li></a>";
        }
        echo "</ul>";

    }
}
elseif(!empty($_GET['searching_data']) && isset($_GET['search_result'])){
    // making query
    $query = 'SELECT book.*,
    author.name as author,
    publisher.name as publisher,
    format.name as format,
    genre.name as genre
    FROM book
    JOIN author USING(author_id)
    JOIN publisher USING(publisher_id)
    JOIN format USING(format_id)
    JOIN genre USING(genre_id)
    where title LIKE :searching_data';
    //prearpring the paramters
    $params = array(':searching_data'=> "%".$_GET["searching_data"]."%");
    //preparing the query
    $stmt = $dbh->prepare($query);
    // execute the query
    $stmt->execute($params);
    // fetching the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // including the book_list php page
    include __DIR__.'/../inc/booklist.php';
}