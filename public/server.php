<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// $dbh = new PDO('mysql:host=localhost;dbname=booksite', 'root', 'root');
$dbh = new PDO('sqlite:' . __DIR__ . '/../inc/booksite.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// check for book_id in request as a get method 
if(isset($_GET['book_id'])) {
    // making query
    $query = 'SELECT book.*,
        author.name as author,
        publisher.name as publisher,
        format.name as format,
        genre.name as genre
        FROM book
        JOIN author USING(author_id)
        JOIN publisher USING(publisher_id)
        JOIN format USING(format_id)
        JOIN genre USING(genre_id)
        WHERE
        book_id = :book_id';
    // parameters array
    $params = array(':book_id' => (int) $_GET['book_id']);
    // preparing query
    $stmt = $dbh->prepare($query);
    //executing array
    $stmt->execute($params);
    // fetching data
    $book = $stmt->fetch(PDO::FETCH_ASSOC);
    // including the detail page
    include __DIR__.'/../inc/detail.php';
} else {
    // making query
    $query = 'SELECT book.*,
    author.name as author,
    publisher.name as publisher,
    format.name as format,
    genre.name as genre
    FROM book
    JOIN author USING(author_id)
    JOIN publisher USING(publisher_id)
    JOIN format USING(format_id)
    JOIN genre USING(genre_id)';
    // parameters array
    $params = array();
    // preparing query
    $stmt = $dbh->prepare($query);
    //executing array
    $stmt->execute($params);
    // fetching data
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // including the book_ list php page
    include __DIR__.'/../inc/booklist.php';
}

// CSRF token 
// Reference: STEVE's code
// generate a csrf token if we need one
if(empty($_SESSION['csrf'])) {
    $_SESSION['csrf'] = md5( uniqid() . time() );
}

// test every POST submission for the csrf token
if('POST' == $_SERVER['REQUEST_METHOD']) {
    if(empty($_POST['csrf']) || $_POST['csrf'] != $_SESSION['csrf']) {
        die('Your session appears to have expired.  CSRF token mismatch!');
    }
}


